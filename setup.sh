#!/bin/bash

set -ex
set -o pipefail

update_cache() {
    apt-get update
}

# install_needed_packages() {
#     sudo apt install -y apt-transport-https ca-certificates curl software-properties-common ca-certificates
# }

# install_and_configure_docker() {
#     curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#     sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
#     sudo apt update
#     sudo apt install docker-ce
#     sudo usermod -aG docker jenkins

#         1  sudo apt update
#     2  sudo apt-get install     apt-transport-https     ca-certificates     curl     gnupg2     software-properties-common
#     3  curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
#     4  sudo apt-key fingerprint 0EBFCD88
#     5  sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/debian \
#    $(lsb_release -cs) \
#    stable"
#     6  sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/debian \
#    $(lsb_release -cs) \
#     7  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
#     8  sudo apt-get update
#     9  sudo apt-get install docker-ce docker-ce-cli containerd.io
#    10  docker ps -a
# }

# install_aws_cli() {
#     apt-get install curl unzip python2.7 python-pip -y
#     curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
#     unzip -o awscli-bundle.zip
#     ./awscli-bundle/install -b ~/bin/aws

# }

# configuring_aws_cli() {
#     /root/bin/aws configure set aws_access_key_id $(AWS_ACCESS_KEY)
#     /root/bin/aws configure set aws_secret_access_key $(AWS_SECRET_ACCESS_KEY)
#     /root/bin/aws configure set default.region us-east-1
#     /root/bin/aws configure set default.output json
# }

install_k6() {
    apt-get install dirmngr --install-recommends
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 379CE192D401AB61
    echo "deb https://dl.bintray.com/loadimpact/deb stable main" | sudo tee -a /etc/apt/sources.list
    apt-get update
    apt-get install k6
}

main() {
    # update_cache
    # install_needed_packages
    # update_cache
    # install_and_configure_docker
    # update_cache
    # install_aws_cli
    # configuring_aws_cli
    update_cache
    install_k6
}

main "$@"


